#!/bin/bash

# Parcourir tous les arguments du script
for arg; do
    # Utilisation de tr pour supprimer tous les caractères qui ne sont pas des voyelles
    nombre_voyelles=$(echo "$arg" | tr -d -c 'aeiouyAEIOUY' | wc -c)
    echo "Nombre de voyelles dans $arg : $nombre_voyelles"
done

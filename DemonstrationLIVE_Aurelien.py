"""

Les variables

"""


name = "" # string
age = 35        # integer
size = 1.75     # float
isMale = True   # boolean


age = 32
age = age + 1
age += 1        # the same as previous line




"""

Les conditions

"""

isRaining = True

clothes = "tshirt et pantalon"

if not isRaining:
  clothes += " et manteau"
  print(clothes)
   

if len(name) > 0:
    print("Bonjour !")
else:
    print("Bonjour !")


if len(name) == 0:
   print("chaine vide")
else:
    if len(name) < 5:
        print("chaine inférieure à 5")
    else:
        print("tous les autres cas")


if len(name) == 0:
    print("0")
elif len(name) < 5:
    print("moins que 5")
elif len(name) < 10:
    print("moins que 10")
else:
    print("les autres cas : 10")



"""

Les boucles

"""

#while True:
#    print("Ceci est sûrement un buuuuug")

aList = [0, 1, 2, 3]

for i in aList:
    print("Ceci est l'itération qui affiche la valeur {i}")

x = 0

while x < 4:
    print("valeur de x : {x}")
    x += 1

print("valeur finale de x : {x}")

print("Bonjour, " + name + ", comment ca va ?")




"""

Les fonctions

"""


def sum(a, b):
    tmp =  float(a) + float(b)
    return tmp


result = sum(len("Je suis numéro "), 1)
print(f"Le résultat est {result}")

print(f"La somme de 1 et 2 est {sum(1,2)}")
print(f"La somme de 1 et 4 est {sum(1,4)}")
print(f"La somme de 3 et 3 est {sum(3,3)}")
print(f"La somme de 2 et 2 est {sum(2,2)}")


a = 10
b = 22
val3 = sum(a, b)
print(val3)




"""

Les objets

"""

# ETAPE 1 : On définit une classe "Computer"...

class Computer:

    """
    ...avec des valeurs internes (des variables, ou des propriétés, définies dans le *constructeur*)
    """
    def __init__ (self, mem, to):
        self.memory = mem
        self.drive = to

    """
    ...et des actions, des fonctions (ou des méthodes)
    """
    def show(self):
        print(f"Je suis un ordinateur de {self.memory}Mo de RAM et de {self.drive}To de disque dur")


# ETAPE 2 : On créée différents ordinateurs

ordinateur1 = Computer(4096, 4)
ordinateur1.show()
ordinateur2 = Computer(8192, 2)
ordinateur2.show()

"""

Les dictionnaires

"""

names = ["Mickael", "François", "Aurélien"]

for p in names:
    print(f"Bonjour {p}")

print(names[1])
print( type(names) )

names.append("Mariola")
print(names)

#######################################################



orel = {
    "firstname":"Aurélien", 
    "lastname":"Chirot", 
    "age":42
}

print(orel["firstname"])
print(orel["lastname"])


for k, v in orel.items():
    print(f"{k} a pour valeur {v}")


################################################
    
annuaire = [

    {
        "firstname":"Aurélien", 
        "lastname":"Chirot", 
        "age":42
    },

    {
        "firstname":"Aurélien", 
        "lastname":"Chirot", 
        "age":42
    },

    {
        "firstname":"Aurélien", 
        "lastname":"Chirot", 
        "age":42
    }
]


print("Bonjour " + annuaire[1]["firstname"] + " !")

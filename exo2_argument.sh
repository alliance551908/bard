#!/bin/bash

# Afficher la phrase avec les arguments
echo "Hello world! My name is $1 $2."

# Afficher le nombre d'arguments
echo "Nombre d'arguments au script : $#"

# Afficher la liste des arguments
echo "Liste des arguments au script :"
echo "$@"

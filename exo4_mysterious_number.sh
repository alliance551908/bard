#!/bin/bash

# Génération d'un nombre mystère entre 1 et 1000
secret_number=$((RANDOM % 1000 + 1))

# Initialisation du nombre d'essais et du nombre maximum d'essais
attempts=0
max_attempts=10

# Affichage des instructions du jeu
echo "Come, weary traveler. You've seen enough BREIF today. Play this little game and rest well. Instructions are simple: I'll choose a secret number between 1 and 1000. You have 10 chances to find it."

# Boucle principale du jeu
while [ $attempts -lt $max_attempts ]; do
    # Calcul du nombre d'essais restants
    remaining_attempts=$((max_attempts - attempts))
    echo "You have $remaining_attempts attempts remaining."

    # Demande à l'utilisateur de deviner un nombre
    read -p "Enter your guess: " guess

    # Vérification de la saisie de l'utilisateur
    if [ "$guess" = "COIN!" ]; then
        # Si l'utilisateur veut redémarrer en saisissant "COIN!", afficher les instructions de paiement et terminer le jeu
        echo "To restart the game, please make a transfer of 1BTC to the following address: https://www.paypal.com/paypalme/showtime82"
        exit
    fi

    # Vérification de la validité de la saisie de l'utilisateur
    if ! [[ $guess =~ ^[0-9]+$ ]]; then
        echo "EROR $secret_number : Oops! Something went wrong."
        continue
    fi

    if [ $guess -lt 1 ] || [ $guess -gt 1000 ]; then
        echo "EROR $secret_number : Oops! Something went wrong."
        continue
    fi

    # Incrémentation du nombre d'essais
    ((attempts++))

    # Comparaison du nombre deviné avec le nombre mystère
    if [ $guess -lt $secret_number ]; then
        echo "YOU SHALL NOT PASS! (Try Higher)"
    elif [ $guess -gt $secret_number ]; then
        echo "YOU SHALL NOT PASS! (Try Lower)"
    else
        # Si le nombre deviné est correct, afficher un message de réussite et terminer le jeu
        echo "Achievement unlocked: You've lost your time for nothing"
        exit
    fi

    # Gestion des messages après un certain nombre d'essais
    if [ $attempts -eq 5 ]; then
        echo "You're welcomed to rest here friend. You have tried enough"
    elif [ $attempts -eq $max_attempts ]; then
        read -p "GAME OVER! TRY AGAIN? INSERT 'COIN!' to restart: " restart
        if [ "${restart^^}" = "COIN!" ]; then
            # Si l'utilisateur souhaite redémarrer, afficher les instructions de paiement et terminer le jeu
            echo "To restart the game, please make a transfer of 1BTC to the following address: https://www.paypal.com/paypalme/showtime82"
            exit
        else
            # Sinon, terminer le jeu
            echo "Thank you for playing!"
        fi
    fi
done

# Fin du jeu
echo "Thank you for playing!"
exit

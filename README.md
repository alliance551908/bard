## Récupération et modification du nom d'utilisateur et mot de passe sur une machine Linux

Si l’on vient à oublier son nom d’utilisateur sur une machine Linux, suivez les étapes suivantes :

| Étape | Description |
|------|-------------|
| 1 | Au moment de l'écran bleu du boot de la VM, appuyez sur `e` |
| 2 | Effacez la partie `ro quiet` et écrivez à la place `rw init=/bin/bash` |
| 3 | Appuyez sur `Ctrl + X` |
| 4 | Tapez `cat /etc/passwd` (affichera le nom d’utilisateur) |
| 5 | Tapez `passwd 'username'` (pour modifier le mot de passe de l’utilisateur) |
| 6 | Tapez `sudo passwd root` (pour modifier le mot de passe du root) |

# Commandes Git

Voici un tableau des commandes Git avec une explication simple pour chacune :

| Commande | Description |
|----------|-------------|
| `git commit` | Enregistre les modifications de fichiers dans le dépôt Git. Crée un nouveau commit avec les changements effectués depuis le dernier commit. |
| `git branch "branch_name"` | Crée une nouvelle branche avec le nom spécifié à partir du commit actuel. |
| `git merge "branch_name"` | Fusionne la branche spécifiée dans la branche actuelle. |
| `git rebase "branch_name"` | Réapplique les commits de la branche spécifiée sur la branche actuelle. |
| `git checkout "branch_name" or "CommitX"` | Déplace vers une branche spécifique ou un commit spécifique. Si une branche est spécifiée, déplace sur cette branche. Si un commit est spécifié, déplace sur ce commit en mode détaché. |
| `git branch -f "branch_name" "CommitX"` | Déplace la branche spécifiée sur le commit spécifié, écrasant la référence précédente de cette branche. |
| `git revert "CommitX"` | Crée un nouveau commit qui annule les modifications apportées par le commit spécifié. |
| `git cherry-pick <CommitX> <CommitY> <...>` | Applique les commits spécifiés à la branche actuelle. |
| `git commit --amend` | Modifie le dernier commit en y ajoutant de nouveaux changements. |
| `git tag "tag_name" "CommitX"` | Crée un tag (étiquette) pour le commit spécifié. |
| `git describe <ref>` | Donne une référence humainement lisible qui décrit l'emplacement du commit spécifié par rapport aux tags les plus récents. |
| `git reset "CommitX"` | Réinitialise la branche courante au commit spécifié, retirant les commits effectués après `CommitX`. Les commits retirés sont placés dans la zone de travail. |
| `git rebase -i "branch_name"~X` | Fait un rebase interactif jusqu'au Xème commit avant celui spécifié. |
| `git reset "CommitX"~X` | Déplace la branche actuelle sur le commit X commits en arrière par rapport au commit spécifié. |
| `git checkout "branch_name" or "CommitX"^^` | Déplace vers le commit parent du parent du commit spécifié. |
| `git checkout "branch_name" or "CommitX"^` | Déplace vers le commit parent du commit spécifié. |
| `git checkout "branch_name" or "CommitX"~X` | Déplace X commits en arrière par rapport au commit spécifié. |
| `git branch -f "branch_name" "CommitX"~X` | Déplace la branche spécifiée sur le commit X commits en arrière par rapport au commit spécifié. |


# Guide d'utilisation des scripts Bash

## Lancer un script

Pour lancer un script Bash, utilisez la commande suivante :

```
./nom_du_script.sh

Assurez-vous que le script a l'autorisation d'exécution (chmod +x nom_du_script.sh) pour pouvoir l'exécuter directement.

## Extension de fichier Bash
Les scripts Bash ont généralement l'extension .sh. Cela permet de les identifier facilement comme des scripts Bash.

## En-tête de fichier Bash
Les scripts Bash doivent commencer par un en-tête spécifiant le chemin vers l'interpréteur à utiliser. Par convention, l'en-tête est le suivant :

#!/bin/bash


